package com.mycompany.implementacja;

import com.mycompany.class_loader.ClassLoaderPow;
import static java.lang.Math.*;


    
    public class Implementacja extends ClassLoaderPow{
        public double liczba1;
        public double liczba2;
        public int znak;
        
//        public Implementacja(){
//            OdpalanieCL();
//        }
        
        public void pokaz_zmienne(){
            System.out.print(this.liczba1);
            System.out.print(this.liczba2);
            System.out.print(this.znak);

        }
        
        public void konwerter(String kod){
                        
            if (kod.contains("+")){
                this.znak=1;
                String[] parts = kod.split("\\+");
                String lp1 = parts[0]; 
                String lp2 = parts[1];
                double l1 = new Double(lp1).doubleValue();
                double l2 = new Double(lp2).doubleValue();
                this.liczba1 = l1;
                this.liczba2 = l2;
            }
            if (kod.contains("-")){
                this.znak=2;
                String[] parts = kod.split("-");
                String lp1 = parts[0]; 
                String lp2 = parts[1];
                double l1 = new Double(lp1).doubleValue();
                double l2 = new Double(lp2).doubleValue();
                this.liczba1 = l1;
                this.liczba2 = l2;
            }
            if (kod.contains("*")){
                this.znak=3;
                String[] parts = kod.split("\\*");
                String lp1 = parts[0]; 
                String lp2 = parts[1];
                double l1 = new Double(lp1).doubleValue();
                double l2 = new Double(lp2).doubleValue();
                this.liczba1 = l1;
                this.liczba2 = l2;
            }
            if (kod.contains("/")){
                this.znak=4;
                String[] parts = kod.split("/");
                String lp1 = parts[0]; 
                String lp2 = parts[1];
                double l1 = new Double(lp1).doubleValue();
                double l2 = new Double(lp2).doubleValue();
                this.liczba1 = l1;
                this.liczba2 = l2;
            }
            if (kod.contains("pow")){
                String[] parts = kod.split("pow");
                String lp1 = parts[0]; 
                String lp2 = parts[1];
                double l1 = new Double(lp1).doubleValue();
                double l2 = new Double(lp2).doubleValue();
                double arglist[]= new double[5];
                arglist[0]=l1;
                arglist[1]=l2;
                
                OdpalanieCL(arglist);
            }
           
        }
        
        public double dodaj(double a, double b){
            return a+b;
        }
        
        public double odejmij(double a, double b){
            return a-b;
        }
        
        public double pomnoz(double a, double b){
            return a*b;
        }
        
        public double podziel(double a, double b){
            return a/b;
        }
        
        
        
        public double akcja(){
            double wynik=0;            
        switch (this.znak) {
            case 1:
                wynik=dodaj(this.liczba1, this.liczba2);
                System.out.print(wynik);
                System.out.print("\n");
                break;

            case 2:
                wynik=odejmij(this.liczba1, this.liczba2);
                System.out.print(wynik);
                System.out.print("\n");
                break;

            case 3:
                wynik=pomnoz(this.liczba1, this.liczba2);
                System.out.print(wynik);
                System.out.print("\n");
                break;

            case 4:
                wynik=podziel(this.liczba1, this.liczba2);
                System.out.print(wynik);
                System.out.print("\n");
                break;
            

        }
        
        
//        System.out.print(wynik);
        return wynik;
        }
    }
